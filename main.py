#!/usr/bin/env python

import json as J
from pathlib import Path
from subprocess import run, PIPE
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class MyApp():
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("Main.glade")

        self.window = builder.get_object("app")
        self.filter = builder.get_object("filter")
        self.json = builder.get_object("json")
        self.out = builder.get_object("out")
        self.filter.connect("changed", lambda _: self.checkJq())
        self.json.connect("key-release-event", lambda _, __: self.checkJq())
        self.config_path = Path.home().joinpath(".config", "jqpy", "config.json")

    def run_jq(self, filter, json):
        proc = run(["jq", filter], stdout=PIPE, stderr=PIPE,
                   input=json, encoding='utf-8')
        if proc.returncode != 0:
            return proc.stderr
        return proc.stdout

    def get_json(self):
        inp_buf = self.json.get_buffer()
        (start, end) = inp_buf.get_bounds()
        return inp_buf.get_text(start, end, False)

    def checkJq(self):
        filterVal = self.filter.get_text()
        jsonVal = self.get_json()
        if len(filterVal) > 0 and len(jsonVal) > 0:
            msg = self.run_jq(filterVal, jsonVal)
            self.out.get_buffer().set_text(msg, len(msg))

    def load_configs(self):
        if self.config_path.exists():
            with open(self.config_path, 'r') as f:
                config = J.load(f)
                json = config['json']
                self.json.get_buffer().set_text(json, len(json))
                self.filter.set_text(config['filter'])

    def save_configs(self):
        json = self.get_json()
        filter = self.filter.get_text()
        if not self.config_path.parent.exists():
            self.config_path.parent.mkdir()
        with open(self.config_path, 'w') as f:
            J.dump({'filter': filter, 'json': json}, f)

    def stop(self):
        self.save_configs()
        Gtk.main_quit()

    def start(self):
        self.load_configs()
        self.window.connect("destroy", lambda _: self.stop())
        self.window.show_all()
        self.checkJq()

        Gtk.main()


if __name__ == "__main__":
    app = MyApp()
    app.start()
